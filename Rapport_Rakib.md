# Rapport de projet : Zork FR #

## Nom code du jeu : Project Abysmal, a java based Game ##

| NOM Prénom     | Numéro Étudiant | Pseudonyme |
| :------------- | :-------------- | :--------- |
| SHEIKH Rakib   | 11502605        | NoobZik    |
| Rayan Fraj     | 11607212        |            |

**Licence 2, Informatique, Institut Galilée.**

*   **Rédaction : NoobZik**
*   Développement par :
    *   [Rakib SHEIKH (NoobZik-Dev)](http://noobzik.ml).
    *   Rayan Fraj

Lien du répertoire Git : [Lien Bitbucket](https://bitbucket.org/asiat/zork)
*Le lien sera public dès le dépôt du projet sur l'ENT*

# CHANGELOG #
*   Ajout d'une classe navigo.
*   Mise sous forme d'inheritence.
*   Ajout des contrôleurs de billets (Joueur AI).
*   Modélisation naïf des pièces mobiles (Tramway).
*   Ajout de diverses commentaires.
*   Simplification de diverses fonctions.
*   Ajout d'une troisème commande.
*   Ajout de la commande montrer / valider / attendre.
*   Fixed sorties.set().

## Compilation ##

Le jeu se compile et se lance de la manière suivante :

```bash
$ make
$ make run
```

**Avec l'absence d'un backlog (ou feedback) des éventuelles problèmes de compilation et/ou erreurs à l'éxecution de votre part pour le premier dépot de projet. Vous déclarez que vous n'avez pas rencontré la moindre bug et la moindre erreur d'execution ou de comportement anormal pour la version précédente.**

<div class="page-break"></div>

# Description du jeu #

### Présentation générale ###

Vous êtes en période de vacance, mais vous avez choisi de travailler pour l'UFR SMBH dans le but de pouvoir payer votre loyer CROUS et d'acheter les pâtes Carrefour Discount situé à Drancy Avenir. Étant donné que le CROUS n'est pas capable de vous envoyer la bourse dans les temps.

Le but de ce jeu est de récolter un certain nombre d'objet dont le poids totale devra être exactement le même que celui généré par le jeu afin de bénéficier de la paye.

Afin d'accomplir ce jeu :

1)    Vous devrez tout d'abord récupérer votre carte étudiante chez vous en prenant le tramway.
2)    Vous reviendrez ensuite à la fac pour commencer à récolter les objets dont le poids totale sera égale à celui demandé par la secrétaire.
3)    Vous devez ensuite aller au secrétariat pour déposer vos objets avec la commande ***check***

## Plan du jeu ##

![Carte](markdown/Map.jpg)

*Carte du jeu détaillé*

## Scénario de test ##

Je ne peux pas vraiment prévoir un scénario de test pour gagner rapidement, puisque les objets sont générés aléatoirement pour chaque pièce. De plus, le joueur n'a rien dans son sac au début du jeu. Il faudra un peu de recherche pour avoir le poids exacte exigé par la secrétaire.

En revanche, un scénario rapide pour perdre est d'aller au secrétariat et de
taper :

```bash
-> check
```
3 fois d'affilé.

# Modification apporté au jeu #

## Description de la version initiale (template) ##

La version initiale avant le début du devoir permet uniquement de se déplacer dans les différentes pièces de l'Institut Galilée a savoir :
*   Le bâtiment C.
*   Devant le bâtiment C.
*   En salle de TD du bâtiment G.
*   Au Secrétariat..
*   Dans la Taverne.

Les commandes du jeu sont déjà disponibles, mais leur utilisation reste complexe à comprendre pour une prise en main rapide. Il est prévu de refactorer le code.

## Description de la version amendé ##

Je ne savais pas quoi rajouter, vu que le projet était considéré comme presque fini en seulement quelques jours. Je préfère me consacrer à d'autres projets qui sont largement plus intéressants (Structures Données Algorithmes) plus tôt que de reprendre à régler ce projet.

*   Il y a eu l'ajout des tramway et donc une gare supplémentaire qui est la Courneuve. A chaque tour, le tramway se déplace d'une gare à l'autre.
*   Dans ce tramway, il à eu l'implémentation des valideur navigo.
*   En conséquence direct, une classe navigo est créée pour gérer les validations.
*   Les joueurs de type intelligence artificielle naïf (random) sont implémentés. Ce sont les contrôleurs de billets. Ils sont là pour assurer la validation de tout titre de transport.
*   Pour pouvoir gagner il faut maintenant être en possession de la carte étudiante, il faudra le récupérer à la maison. Elle se situe à l'arrêt La Courneuve.
*   La commande *valide* est seulement disponible dans le tramway.
*   La commande *montrer* doit avoir deux argument

### Modifications (historique des commit dans les grandes lignes) ###

*   **c6341ffacd48003bf3e55fac7ffb0de0631e5850**
    *   Modification de l'encodage du javadoc pour le rendre compatible pour toutes les navigateurs (Issue #1).

*   **7395d17333db9cba6b44bf5a745f46fca5f2777**
    *   Re-positionnement des champs de class de Joueur.java

*   **64975027d216bcac138788b96cbb868c8400c62c**
    *   Mise en inheritence du Zork par la création d'une classe parent NamingDescription. Les méthodes et les champs répétitifs sont supprimés pour en faire une inheritence. (Basé sur les cours de Udacity).
    *   Le HashCode du ObjectZork.java à été modifié pour le rendre compatible avec l'inheritence.
    *   Des méthodes ont été renommés dans Joueur.java pour éviter toute ambiguïté et fixer toutes erreurs lié à l'accès aux champs parent
    (setWeight -> setAppendWeight, this.weight -> this.getWeight).

*   **f0b6f9ad264f2aa49ac87e873a2939e308e7d754**
    *   Même chose, les problèmes d'accès aux champs ont été réglés par la création des méthodes supplémentaires pour Joueur.java (this.get\*).

*   **bf4df8b2ce17096d5de41e767dc1da956bb3d387**
    *   Presque toutes les boucles whiles répétitifs sont fixés sur une seule ligne grâce aux accès direct.
    ```java
    // Piece.java
    public boolean objectInList (ObjectZork o);
    public void    removeObject (ObjectZork o);
    public void    removeUser   (Joueur j);
    // Jeu.java
    public boolean itemInArray  (ObjectZork o)
    ```
*   **5da72f6062c250de6d1ae8fc4920279d2a1cb44f**
    *   Création de la classe CarteNavigo et de joueurAI.
    *   **Carte Navigo** : Elle permet de modéliser le futur titre de transport pour pouvoir prendre le Tramway plus tard.
    *   **JoueurAI** : Elle permet de représenter les joueurs gérés par l'intelligence artificielle.
    *   L'idée de faire CarteNavigo et contrôleur de billet est venu de quelqu'un de la promotion veut tout prix, éviter les contrôleurs du T11 qui sont omniprésent. Il a déjà écopé d'une amende et cette personne est récidiviste.
    *   Un tramway est considéré comme une pièce. La classe Piece.java a été modifié pour prendre en charge les valideurs de titre de transport spécifiquement à cette pièce.

*   **5562da2fcc2cc91687bf67c2874b1cba25a1f05a**
    *   Ajout d'un champ CarteNavigo dans Joueur.java
    *   On suppose donc que tout jour possède un titre de transport.

*   **05d80c3751d8cf5a0bef9a1a4947b0270e05f328**
    *   Début d'implémentation du tramway dans Jeu.java et ajout d'une nouvelle pièce Maison.
    *   Nouvelle gare : La Courneuve.
    *   Certaines pièces ont été promu en tant que variable globale afin de mieux gérer les sorties du tramway.

*   **d3254a4ff404a416c5930a015f17095f7bcf7305**
    *   Introduction de 3 nouvelles commandes qui sont *valider, montrer, attendre*
    *   Ajout d'un troisème argument de commande dans l'Analyseur Syntaxique.

*   **87380fe604cffe61f27beebffd0f5a8500c07b47**
    *   Diverses corrections de différents noms des méthodes
    *   Suppresion d'une boucle while dans :
    ```java
    public void removeItem(ObjectZork o);
    public boolean doIhaveIt(ObjectZork o)
    ```

*   **9c57e21bab58440961fa9e35911941187025bc3e**
    *   Modification de setSorties, pour vider les sorties disponibles à chaque appelle à cette méthode. Elle est requise pour le tramway et les gares. (Piece mobile / Sorties dynamique).

*   **8d42f05c7e91e736010385df37510d19aed92d9e**
    *   Diverses correction d'orthographe
    *   Implémentation (prototype) des JoueurAI.
    *   Ajout d'un champs argent pour le Joueur et ses méthodes associés.
    *   Modification de piece pour avoir une liste des Joueurs et JoueursAI actuelle et ses méthodes associés pour les gérer.

*   **4477288dfb53863fb38c6e145c3882780603806c**
    *   Création d'un nouveau objet Zork (La carte Etudiante).
    *   Modification de la condition pour gagner (Il faut dorénavant posséder la carte étudiante pour pouvoir utiliser la commande *check* au secrétariat).

<div class="page-break"></div>

## Liste des commandes ##

-   retour : *permet de retourner dans la pièce précédent, seulement une fois.
Il est largement possible qu'il y a une boucle infinie si la pièce précédente
est une pièce de téléportation.*
-   cplayer :*permet d'afficher les informations du joueur actuelle.*
-   bag : *permet de lister le contenue du sac, son poids actuelle, et son poids maximale.*
-   prendre : *permet de prendre un objet depuis pièce → sac.*
```bash
# Utilisation de la commande
-> prendre <Objet> # Astuce, utilisez "lister" pour avoir la liste complète des
                   # objets disponibles dans la pièce.
                   # Le nom de l'objet doit être correctement orthographié y
                   # compris la case qui est sensitive.
```
-   laisser : *permet de laisser un objet depuis sac → pièce.*
```bash
# Utilisation de la commande
-> laisser <Objet> # Pareil pour prendre mais la commande pour lister est "bag"
```

-   lister : *permet de lister les objets qui sont dans la pièce.*
-   check : *permet de déposer le sac au Secrétariat, utilisable 3x.*
-   but : *permet de rappeler au joueur le poids exact du sac pour gagner.*
-   sorties : *permet de lister toutes les sorties possible de la pièce.*
-   montrer : *permet de montrer sa carte navigo au controleur de billet.*
```bash
# Utilisation !
-> montrer navigo RATP
```
-   valider : *permet de valider sa carte étudiante.*
-   attendre : *permet d'attendre / passer le tour Cette commande est à utiliser lorsque on attends le tramway ou bien pour faire bouger le tramway d’une gare à l’autre.*

Ce qu'on peut faire avec ce jeu, c'est de jouer normalement sans pression jusqu'à avoir le poids du sac égal à celui qui est généré automatiquement pour gagner à condition de posséder la carte étudiante.


<div class="page-break"></div>


# Tableau des pièces #
| Nom de la pièce  | Transportable | Poids  |
| :--------------  | :-------------| :----- |
| diskPoids        | Oui           | 50 kg  |
| Cours (De PACES) | Oui           | 10 kg  |
| Chaise           | Non           | 900 kg |
| Voitures         | Non           | 900 kg |
| Haribo           | Oui           | 2 kg   |
| Velleda          | Oui           | 20 kg  |
| Cannette1        | Oui           | 2 kg   |
| Cannette2        | Oui           | 2 kg   |
| Livre            | Oui           | 2 kg   |
| Table            | Non           | 20 kg  |
| Katana           | Oui           | 20 kg  |
| PC               | Oui           | 10 kg  |
| EcranPC          | Oui           | 5 kg   |
| SourisPC         | Oui           | 2 kg   |
| ClavierPC        | Oui           | 2 kg   |
| carteEtudiante   | Oui           | 0 kg   |
