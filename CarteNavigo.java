/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   CarteNavigo.java                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Feroujdz <rayan77420@hotmail.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/18 22:25:53 by Feroujdz          #+#    #+#             */
/*   Updated: 2017/11/18 23:14:02 by Feroujdz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
 * <p> Carte Navigo </p>
 *
 * <p>Cette objet à part représente un titre de transport en Ile-de-France.
 * Il comporte un booléen qui permet de savoir si ce passe est validé ou non</p>
 *
 * @author Rayan Fraj
 */

public class CarteNavigo {
  private boolean validation;

  /**
   * Default constructor
   * @author Rayan Fraj
   */
  public CarteNavigo () {
    this.validation = false;
  }

  /**
   * Return the current state of validations
   * @author Rayan Fraj
   * @return boolean statement
   */
  /*@
    @ pure
    @*/
  public boolean getValidation () {
    return validation;
  }

  /**
   * Set validation to true:
   * @author Rayan Fraj
   */
  /*@
    @ ensures validation = true
    @*/
  public void setValidation () {
    this.validation = true;
  }

  /**
   * resetValidation to false
   * @author Rayan Fraj
   */
  /*@
    @ ensures validation = false;
    @*/
  public void resetValidation () {
    this.validation = false;
  }

}
