/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Piece.java                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/04 17:21:48 by NoobZik           #+#    #+#             */
/*   Updated: 2017/11/22 17:16:07 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

import java.util.Set;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Objects;

/**
 *  A Piece (Room) class for Zork Game <p>
 *
 *  Cette classe fait partie du logiciel Zork, un jeu d'aventure simple en mode
 *  texte.</p> <p>
 *
 *  A Piece class represent a rooom inside the Zork. It is linked up to six Room
 *  definied as exits path. The exits are listed as "Nord", "Est", "Sud", "Haut"
 *  and "Bas". For each exits, we can have a another room or a null statement
 *  </p>
 * @author     Rayan
 * @author     Michael Kolling (Source)
 * @author     NoobZik
 * @version    0.0.3
 */

public class Piece {
	private String                description;
	private Map<String,Piece>     sorties;
	private ArrayList<ObjectZork> aO;
	private ArrayList<Joueur>     currentUsers;
	private ArrayList<JoueurAI>   currentAi;


	/**
	 *  Initialise une piece décrite par la chaine de caractères spécifiée.
	 *  Initialement, cette piece ne possède aucune sortie. La description fournie
	 *  est une courte phrase comme "la bibliothèque" ou "la salle de TP".
	 * @author Rayan
	 * @param  description  Description de la piece.
	 */
	/*@ pure */
	public Piece(String description) {
		this.description  = description;
		this.sorties      = new HashMap<String, Piece>();
		this.aO           = new ArrayList<ObjectZork>();
		this.currentUsers = new ArrayList<Joueur>();
		this.currentAi    = new ArrayList<JoueurAI>();

	}


	/**
	 * Define exits of the room. Each path is either linked to an another room or
	 * a null statement for no room.
	 * This method called during the creation of a new instance of Piece.
	 * Code a REFACTORER EN 5 ligne
	 * @param  n  North exit path
	 * @param  e  East exit path
	 * @param  s  South exit path
	 * @param  w  West exit path
	 * @param  h  Upper level exit path
	 * @param  d  Down level exit path
	 * @author Rayan
	 */
	/*@ pure */

	public void setSorties(Piece n, Piece e, Piece s, Piece w, Piece h, Piece d) {
		this.sorties.clear();
		if (n != null) {
			sorties.put("nord", n);
		}
		if (e != null) {
			sorties.put("est", e);
		}
		if (s != null) {
			sorties.put("sud", s);
		}
		if (w != null) {
			sorties.put("ouest", w);
		}
		if (h != null) {
			sorties.put("haut", h);
		}
		if (d != null) {
			sorties.put("bas", d);
		}
	}


	/**
	 *  Renvoie la description de cette piece (i.e. la description spécifiée lors
	 *  de la création de cette instance).
	 *
	 * @return    Description de cette piece
	 */
	/*@ pure @*/
	public String descriptionCourte() {
		return description;
	}


	/**
	 *  Renvoie une description de cette piece mentionant ses sorties et
	 *  directement formatée pour affichage, de la forme: <pre>
	 *  Vous etes dans la bibliothèque.
	 *  Sorties: nord ouest</pre> Cette description utilise la chaine de caractères
	 *  renvoyée par la méthode descriptionSorties pour décrire les sorties de
	 *  cette piece.
	 * @author Rayan
	 * @return    Description affichable de cette piece
	 */
	/*@ pure @*/
	public String descriptionLongue() {
		return "Vous etes dans " + description + ".\n" + descriptionSorties();
	}


	/**
	 *  Renvoie une description des sorties de cette piece, de la forme: <pre>
	 *  Sorties: nord ouest</pre> Cette description est utilisée dans la
	 *  description longue d'une piece.
	 * @return    Une description des sorties de cette pièce.
	 */
	/*@ pure @*/
	public String descriptionSorties() {
		String resulString = "Sorties disponible: ";
		Set<String> keys   = sorties.keySet();
		Iterator<String> i = keys.iterator();

		while ( i.hasNext() ) {
			resulString += " " + i.next();
		}
		return resulString;
	}

	/**
	 * Return the size of the ArrayList of the piece. Can be translated into
	 * return the amount of object contained inside the piece.
	 * @return size integer
	 * @author Rayan
	 */
	 /*@ pure */
	public int getSize() {
		return aO.size();
	}

	/**
	 * Return an object located inside the ArrayList of the piece
	 * @param i The position of the desired object
	 * @return  The object at the i position of the ArrayList
	 * @author Rayan
	 */
	 /*@ pure */
	public ObjectZork getObject(int i) {
		return aO.get(i);
	}

	/**
	 *  Renvoie la piece atteinte lorsque l'on se déplace a partir de cette piece
	 *  dans la direction spécifiée. Si cette piece ne possède aucune sortie dans cette direction,
	 *  renvoie null.
	 *
	 * Return the targetted piece while attempting to move on it. If the direction
	 * target a void piece, it will simply return null
	 * @param  direction  The path of the target piece
	 * @return            Targetted piece or null statement
	 * @author Rayan
	 */
	 /*@ pure @*/
	public Piece next(String direction) {
		return sorties.get(direction);
	}

	/**
	 * Add a Zork Object into the ArrayList aO
	 * @param o A Zork Object
	 */
	/*@ pure @*/
	public void addObject(ObjectZork o) {
		aO.add(o);
	}

	/**
	 * Remove a Zork Object from the ArrayList aO
	 * @param o A ZorkObject
	 */
	/*@ pure @*/
	public void removeObject(ObjectZork o) {
		if (objectInList(o)) {
			 this.aO.remove(o);
		 }
	}

	/**
	 * [objectInList description]
	 * @param  o          [description]
	 * @return            [description]
	 */
	public boolean objectInList (ObjectZork o) {
		return (this.aO.contains(o)) ? true : false;
	}

	/**
	 * Add a user into the piece database
	 * @param j [description]
	 */
	public void addUser (Joueur j) {
		this.currentUsers.add(j);
	}

	/**
	 * Remove an user from the piece database
	 * @param j [description]
	 */
	public void removeUser (Joueur j) {
		this.currentUsers.remove(j);
	}

	public void addAI (JoueurAI j) {
		this.currentAi.add(j);
	}

	public void removeAI (JoueurAI j) {
		this.currentAi.remove(j);
	}

	public JoueurAI getAI () {
		return this.currentAi.get(0);
	}

	public Joueur getUser () {
		return this.currentUsers.get(0);
	}

	public void showPlayerOnPiece () {
		int i = -1;
		while (++i < currentAi.size()) {
			currentAi.get(i).showPlayer();
		}
	}

	/*** Tramway ***/

	/**
	 * Call setValidation
	 * @param c CarteNavigo type
	 */
	 /*@
		@ pures
		@*/
	public void valideur(CarteNavigo c) {
		c.setValidation();
	}

	/**
	 * get the description
	 * @return String formatted description
	 */
	/*@
		@ pures
		@*/
	public String getDescription() {
		return this.description;
	}

	/**
	 * Return all the exit possible
	 * @return Map
	 */
	/*@
		@ pures
		@*/
	public Map<String,Piece> getSortie() {
		return this.sorties;
	}
	/**
	 * Compare two pieces
	 * If the description are equals, so return true statement.
	 * @param  o      The 2nd object to compare with.
	 * @return        A boolean statement
	 */
	 /*@ pure
		 @ ensures !(o instanceof Piece) ==> !\result;
		 @ ensures  (o instanceof Piece) ==> !\result;
		 @ \result == (descriptionCourte() == ((Piece) p).descriptionCourte()));
		 @*/
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Piece)) {
			return false;
		}

		Piece p = (Piece) o;

		return ( this.descriptionCourte().equals(p.descriptionCourte()) ) ?
			true : false;
	}

	/**
   * Since we have a customized equals, we have to customize also the hashCode
   * since equals and hashcode are close related.
   * You can find some information about hashCode into java.util.Hashtable
   * IF YOU EDIT EQUALS, YOU ALSO HAVE TO EDIT HASHCODE
   * @return A generated hashCode
   */
  /*@ pure @*/
	@Override
	public int hashCode() {
		return Objects.hash(description, sorties, aO);
	}
}
