/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Abysmal.java                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/04 17:22:06 by NoobZik           #+#    #+#             */
/*   Updated: 2017/10/27 10:38:40 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

import java.util.*;
/**
 * Main entry point for zork game
 */
public class Abysmal {
    public static void main (String args[]) {
		Jeu leJeu;

		leJeu = new Jeu();
		leJeu.jouer();
    }
}
