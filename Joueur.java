/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Joueur.java                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/18 17:27:40 by NoobZik           #+#    #+#             */
/*   Updated: 2017/11/22 17:18:20 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

import java.util.ArrayList;

/**
 * This class is just about how a Player is defined<p>
 *
 * The following object is defined by :</p>
 * - <br> A ArrayList of bag (Representing arrays of ObjectZork)
 * - <br> A name identifier followed by his description
 * - <br> An integer maxWeight
 * - <br> An integer weight
 * - <br> An ArrayList of ObjectZork
 *
 * <p> This class containt all methods that are required to manage Joueur
 * instances. The Constructor init bag, maxWeight at 150 and the current
 * weight at 0 </p>
 * @author Rakib SHEIKH NoobZik
 * @since v0.0.2
 * @author Rayan
 */

public class Joueur extends NamingDescription {
	private ArrayList<ObjectZork> bag;
	private int                   maxWeight;
	private int                   money;
	private CarteNavigo           navigo;

	/**
	 * Use this Constructor to create new players.
	 * The current max carryable weight is set to 150 kg.
	 * And the current weight is set to 0.
	 * @param n A name of the player.
	 * @param d A description of the player.
	 * @param i Amount of money
	 * @author Rayan
	 */
	 /*@
	  @ requires (n != null && d != null);
		@ invariant maxWeight = 150
		@ pure
		@*/
	public Joueur(String n, String d, int i) {
		super(n, d);
		this.bag         = new ArrayList<ObjectZork>();
		this.money      = i;
		this.maxWeight   = 150;
		this.navigo      = new CarteNavigo();
	}



	/**
	 * Add a Zork ObjectZork into the bag of the player.
	 * @author Rayan
	 * @param o An Zork Object.
	 */
	/*@
	  @ requires (o != null);
		@ pure
	  @*/
	private void addItem(ObjectZork o) {
		int w = o.getWeight();

		this.bag.add(o);
		this.setAppendWeight(w);
	}

	/**
	 * Remove the lasted Zork Object from the bag of the player.
	 * Also reduce the weight of the current bag.
	 */
	/*@
	  @ ensures \result w < 0;
		@ ensures \result (* item removed *);
	  @ pure
		@*/
	public void removeLastItem() {
		int        pos = this.bag.size() - 1;
		ObjectZork o   = this.bag.get(pos);
		int        w   = 0 - o.getWeight();

		this.setAppendWeight(w);
		this.bag.remove(pos);
	}

	/**
	 * Remove a item from the bag. Use a while statement to search the Object
	 * inside the bag, removes it and apply the new weight of the bag by negating
	 * the current weight of the object.
	 * @param  o The Zork Object to remove.
	 */
	/*@
	  @ requires (o != null);
		@ pure
		@*/
	public void removeItem(ObjectZork o) {
		int w = 0 - o.getWeight();

		if (this.bag.contains(o)) {
			this.bag.remove(o);
			this.setAppendWeight(w);
		}
		return;
	}


	/**
	 * Retrive the Maximum carryable weight of the player.
	 * @return Max weight carryable.
	 */
	/*@
 	  @ ensures (* this.maxWeight not nulled *);
 	  @ pure
 	  @*/
	public int getMaxWeight() {
		return this.maxWeight;
	}

	/**
	 * Method that check if the selected Zork Object can be carried or nah.
	 * If yes, stack it to the bag.
	 * If not, just tell him nicely to get the fuck off.
	 * @param o A Zork Object.
	 * @return  A boolean statement for successfull insert or nah.
	 */
	/*@
 	  @ requires o != null
		@ invariant w1 = getWeight();
		@ invariant w2 = this.weight;
		@ invariant w0 = this.maxWeight;
		@ ensures \result (* true if w2+w1 <= w0 *);
		@ ensures \result (* false if w2+w1 > 0  || getCarry == false)
 	  @ pure
 	  @*/
	public boolean isAdd(ObjectZork o) {
		int w0 = this.getMaxWeight();
		int w1 = o.getWeight();
		int w2 = this.getWeight();

		if (o.getCarry() == true) {
			if (w2+w1 <= w0){
				this.addItem(o);
				return true;
			}
		  else {
			System.out.println("L'objet est trop lourd pour être transporté");
			}
		}
		return false;
	}

	/**
	 * Count the occurence of the desired Object into the bag.
	 * @param o Zork Object to check occurence into the bag.
	 * @return count
	 */
	/*@
 	  @ requires o != null;
		@ ensures \result count >= 0;
 	  @ pure
 	  @*/
	public int howManyOf(ObjectZork o) {
		int i = -1;
		int count = 0;

		while (++i < this.getBagSize()) {
			if (this.getBagItem(i).equals(o)) {
				count++;
			}
			continue;
		}
		return count;
	}

	/**
	 * Check into the bag if the desired Zork Object is in the bag of the player
	 * or nah.
	 * @param o A zork object
	 * @return A boolean statement
	 */
	/*@
	 	@ requires o != null;
		@ ensures \result (* true if we find item in ArrayList *);
		@ ensures \result (* false if no item found in ArrayList);
	 	@ pure
	 	@*/
	public boolean doIhaveIt(ObjectZork o) {
		return (this.bag.contains(o)) ? true : false;
	}

	/**
	 * Display the name of the player and his description.
	 */
	/*@
	  @ pures
		@*/
	public void showPlayer() {
		System.out.println(this.getName() + " " + this.getDescription());
	}

	/**
	 * Display all the object contained inside the bag with the addition of the
	 * current weight and the max weight of the bag.
	 */
	/*@
	  @ requires this.bag.size() != null;
	  @ invariant size = this.bag.size();
 	  @ pure
 	  @*/
	public void displayBag() {
		int size = this.getBagSize();
		int i = -1;
		ObjectZork o;

		System.out.println("Le poids du sac : " + this.getWeight() + " kg");
		System.out.println("Le poids maximal :" + this.getMaxWeight() + "kg");
		if (size > 0) {
			System.out.println("Vous avez dans votre sac:");
			while(++i < size){
				o = getBagItem(i);
				System.out.println(o.getName() + " " + o.getDescription());
			}
		}
		else {
			System.out.println("Vous n'avez rien dans vôtre sac");
		}
	}

	/**
	 * Simply return the current size of the bag AKA the amount of object inside
	 * the bag.
	 * @author Rayan
	 * @return a integer bag size.
	 */
	 /*@
 	  @ requires this.bag.size() != null;
 	  @ pure
 	  @*/
	public int getBagSize() {
		return this.bag.size();
	}

	/**
	 * Retrieve the ObjectZork inside the bag given by his position
	 * @param  i   The position of the object inside the bag.
	 * @return     The linked object according to his position inside the bag
	 * @author Rayan.
	 */
	/*@
	  @ requires int i != null
		@ ensures  \result bag.get(i);
	  @ pure
	  @*/
	public ObjectZork getBagItem(int i) {
		return this.bag.get(i);
	}

	/**
	 * Return the current ammount of money
	 * @author Rayan Fraj
	 * @return integer money
	 */
	/*@
	  @ pures
		@*/
	public int getMoney() {
		return this.money;
	}

	/**
	 * Set the desired amount of money
	 * @author Rayan Fraj
	 * @param i integer amount of money
	 */
	/*@
		@ ensures money += i;
		@*/
	public void setMoney (int i) {
		this.money += i;
	}

	public void showMoney () {
		System.out.println("Argent : " + this.money + "euros");
	}

	/**
	 * Return the transportation card of the current holder
	 * @author Rayan Fraj
	 * @return CarteNavigo
	 */
	/*@
		@ pures
		@*/
	public CarteNavigo getNavigo() {
		return this.navigo;
	}


}
