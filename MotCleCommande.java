/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MotCleCommande.java                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/04 17:21:40 by NoobZik           #+#    #+#             */
/*   Updated: 2017/11/22 16:30:16 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
 *  <p>
 *
 *  Command lister class</p> <p>
 *
 *  Classe répertoriant l'ensemble des mots-clé utilisables comme commande dans
 *  le jeu. Elle est utilisée pour vérifier la validité des commandes de
 *  l'utilisateur.
 *
 * @author Rakib SHEIKH NoobZik
 * @author Rayan Fraj
 * @since v0.0.1
 */

public class MotCleCommande {
	/**
	 *  Un tableau constant contenant tous les mots-clé valides comme commandes.
	 */
	private final static String commandesValides[] = {"aller",	"retour",
	 "quitter", "reset", "cplayer", "bag", "prendre", "laisser", "lister",
	 "check", "but", "sorties", "aide", "valider", "attendre", "montrer",
	  "argent"};


	/**
	 *  Initialise la liste des mots-clé utilisables comme commande.
	 */
	public MotCleCommande() { }


	/**
	 *  Check whether a given String is a valid command word. Return true
	 *  if it is, false if it isn't.
	 *
	 * @param  aString  String to test
	 * @return          true if arg is valide, else false.
	 */
	public boolean estCommande(String aString) {
		int i = -1;
		while (++i < cmdSize()) {
			if (commandesValides[i].equals(aString)) {
				return true;
			}
		}
		return false;
	}



	/**
	 *  Display all valide cmds.
	 */
	public void afficherToutesLesCommandes() {
		int i = -1;
		while (++i < cmdSize()) {
			System.out.print(commandesValides[i] + "  ");
		}
		System.out.println();
	}


	/**
	 * Return the size of the command
	 * @return int cmd size
	 */
	public int cmdSize() {
		return commandesValides.length;
	}
}
