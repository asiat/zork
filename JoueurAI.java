/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   JoueurAI.java                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/18 22:13:42 by NoobZik           #+#    #+#             */
/*   Updated: 2017/11/22 17:15:01 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

public class JoueurAI extends NamingDescription {
  /**
   * Un JoueurAI est un controleur de titre de transport.
   *
   * Il est la pour controler le passe navigo. Si le navigo est validé, on fait
   * rien
   * Sinon on soustrait de l'money, -50 € direct
   * @author Rayan Fraj
   */

   /**
    * Default Constructor
    * @author Rayan Fraj
    */
   public JoueurAI () {
     super("RATP", "Agent de controle de billet");
   }

  /**
   * Cette fonction traite les cas où le passe navigo est validé et non validé.
   * Il y a deux sous cas : Assez d'argent pour payer l'ammende et son
   * contraire.
   *
   * @author Rayan Fraj
   * @param  j             [description]
   * @param  c             [description]
   * @return             [description]
   */
  /*@
    @ if get money >= 50
    @   ensures j.setMoney -= 50
    @ else
    @  pures
    @*/
  public int controleNavigo (Joueur j, CarteNavigo c) {
     if (c == null) {
       return -1;
     }
     if (!(c.getValidation())) {
       if (j.getMoney() >= 50) {
         j.setMoney(-50);
         System.out.println("Ammende payé car navigo non validé, merci");
         return 1;
       }
       else {
         System.out.print("Ammende non payé pour manque de fonds, direction");
         System.out.printf(" le tribunal de Bobigny\n");
         return 0;
       }
     }

     System.out.println("Passe navigo validé et en règle, merci");
     return 1;
  }

  /**
   * Print he's name + description
   * @author Rayan Fraj
   */
  /*@
 	  @ pures
 		@*/
  public void showPlayer () {
    System.out.println(this.getName() + " " + this.getDescription());
  }
}
