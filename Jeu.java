/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Jeu.java                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/04 17:21:32 by NoobZik           #+#    #+#             */
/*   Updated: 2017/11/26 20:50:22 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

import java.util.ArrayList;

/**
 * Main entry class for Zork. <p>
 *
 *  Zork est un jeu d'aventure très rudimentaire avec une interface en mode
 *  texte: les joueurs peuvent juste se déplacer parmi les différentes pieces.
 *  Ce jeu nécessite vraiment d'etre enrichi pour devenir intéressant!</p> <p>
 *
 *  Pour jouer a ce jeu, créer une instance de cette classe et appeler sa
 *  méthode "jouer". </p> <p>
 *
 *  Cette classe crée et initialise des instances de toutes les autres classes:
 *  elle crée toutes les pieces, crée l'analyseur syntaxique et démarre le jeu.
 *  Elle se charge aussi d'exécuter les commandes que lui renvoie l'analyseur
 *  syntaxique.</p>
 *
 * @author     Rakib Sheikh NoobZik
 * @author     Rayan Fraj
 * @since      0.0.1
 */

public class Jeu {
	private AnalyseurSyntaxique   analyseurSyntaxique;
	private Piece                 current;
	private Piece                 currentAI;
	private Piece                 last;
	private Piece                 lastAI;
	private ArrayList<Joueur>     player   = new ArrayList<Joueur>();
	private ArrayList<JoueurAI>   playerAI = new ArrayList<JoueurAI>();
	private int                   win      = randomWithRange(10,150);
	private int                   counter  = 0;
	private int                   validatorCounter = 0;
	private boolean               winGame  = false;
	private ArrayList<Piece>      pieceArray;
	private ArrayList<ObjectZork> defaultO = new ArrayList<ObjectZork>(12);
	/* Piece mobiles */
	Piece tt  = new Piece("Tramway T1 (101)");
	Piece dA  = new Piece("à l'arret du T1 Drancy Avenir");
	Piece lC  = new Piece("à l'arret du T1 Courneuve 6 routes");
	Piece pi1 = new Piece("devant le batiment Illustration");
	Piece pi2 = new Piece("devant la tour Illustration");
	Piece TP  = new Piece("Teleportation");
	Piece mA  = new Piece("Maison");
	ObjectZork carteEtudiante = new ObjectZork("Carte", "carteEtudiante", 0, true);
	/**
	 *  Crée le jeu et initialise la carte du jeu (i.e. les pièces).
	 */
	/*@
	 * @ensures analyseurSyntaxique() = AnalyseurSyntaxique;
	 * @ensures ArrayList<Joueur> () = new ArrayList<Joueur>
	 */
	public Jeu() {
		int randomMoney = randomWithRange(0,50);
		this.creerPieces();
		this.analyseurSyntaxique = new AnalyseurSyntaxique();
		this.player = new ArrayList<Joueur>();
		this.playerAI = new ArrayList<JoueurAI>();
		addPlayer("NoobZik", "Part of NoobZik-Dev", randomMoney);
		addPlayerAI();
		current.addUser(player.get(0));
		currentAI.addAI(playerAI.get(0));
	}


	/**
	 *  Crée toutes les pieces et relie leurs sorties les unes aux autres.
	 */
	public void creerPieces() {

		defaultO.add(new ObjectZork("diskPoids", "Un diskPoids transportable de la fac", 50, true));
		defaultO.add(new ObjectZork("Cours", "Si tu perd, au revoir la p2", 10, true));
		defaultO.add(new ObjectZork("Chaise", "Juste des chaise bien rangé", 900, false));
		defaultO.add(new ObjectZork("Voitures", "voitures qui prennent trop de place", 900, false));
		defaultO.add(new ObjectZork("Haribo", "Cest des dragibus miskine", 2, true));
		defaultO.add(new ObjectZork("Velleda", "Une cargaison de 100 feutres", 20, true));
		defaultO.add(new ObjectZork("Cannette1", "Coca Cola", 2, true));
		defaultO.add(new ObjectZork("Cannette2", "Lipton Iced tea", 2, true));
		defaultO.add(new ObjectZork("Livre", "Un livre qui appartient à la BU JD", 2, true));
		defaultO.add(new ObjectZork("Table", "une table fait pour écrire, ou dormir", 20, false));
		defaultO.add(new ObjectZork("Katana", "Selon la légende, ce katana est perdu au sein de la fac", 20, true));
		defaultO.add(new ObjectZork("PC", "L'Unité centrale de l'ordinateur", 10, true));
		defaultO.add(new ObjectZork("EcranPC", "Un ecran d'un ordinateur", 5, true));
		defaultO.add(new ObjectZork("SourisPC", "Une souris banale d'un ordinateur", 2, true));
		defaultO.add(new ObjectZork("Clavier", "un clavier d'un ordinateur", 2, true));


		Piece sTD = new Piece("une salle de TD dans le batiment Illustration");
		Piece iM  = new Piece("la mezazzine Illustration");
		Piece hA  = new Piece("l'amphi Hannah Arendt");
		Piece jG  = new Piece("dans l'amphi Johannes-Gutenberg");
		Piece prk = new Piece("le parking de l'illustration");
		Piece jD  = new Piece("la biliothèque Jean Dausset");
		Piece iC  = new Piece("au centre du couloir de l'Illustration");
		Piece sc  = new Piece("le secrétariat");



		//                     North, East,South, West, Upper, Down
		// Stations du T1
		//
		dA.setSorties          (pi1,   pi2,   TP, null, null, null);
		lC.setSorties          (null,   mA, null, null, null, null);
		//
		// Tramway
		//
		tt.setSorties       (null, lC, null, null, null, null);

		pi1.setSorties         (iC,    prk,   dA, null, null, null);
		pi2.setSorties         (prk,  null, null,   dA, null, null);
		prk.setSorties         (jD,     iC,  pi2,  pi1, null, null);
		iC.setSorties          (prk,  null,  pi1,   hA,   iM, null);
		iM.setSorties          (null,  sTD, null,   sc,   jG,   iC);
		jD.setSorties          (null, null, prk, null, null, null);
		sc.setSorties          (null,   iM, null, null, null, null);
		sTD.setSorties         (null, null, null,   iM, null, null);
		jG.setSorties          (null, null, null, null, null,   iM);
		hA.setSorties          (null,   iC, null, null, null, null);
		mA.setSorties          (null, null, null,   lC, null, null);

		pieceArray = new ArrayList<Piece>();

		addPiece(dA);
		addPiece(pi1);
		addPiece(pi2);
		addPiece(prk);
		addPiece(iC);
		addPiece(iM);
		addPiece(jD);
		addPiece(sc);
		addPiece(sTD);
		addPiece(jG);
		addPiece(hA);

		current = pieceArray.get(0);
		currentAI = tt;
		lastAI = tt;
		randomPieceObjectInsert(pieceArray, defaultO);
	}

	/**
	 * Add player to the game
	 * @param n Name of the player
	 * @param d Description of the player
	 * @param i amount of money
	 */
	//@ pure
	public void addPlayer(String n, String d, int i){
		Joueur j = new Joueur(n,d,i);
		this.player.add(j);
	}

	/**
	 * Add a AI Player
	 */
	public void addPlayerAI(){
		JoueurAI j = new JoueurAI();
		this.playerAI.add(j);
	}


	/**
	 *  Pour lancer le jeu. Boucle jusqu'a la fin du jeu.
	 */
	/*@ pure
	  @ invariant counter
	  @*/
	public void jouer() {
		afficherMsgBienvennue();
		boolean termine = false;
		while (!termine) {
			if (this.current.equals(currentAI)) {
				System.out.println("Controleur RATP, présentez titre de transport svp");
				System.out.println("(Il faudra tapper : montrer navigo RATP)");
			}
			Commande commande = analyseurSyntaxique.getCommande();
			termine = traiterCommande(commande);
		}
		if (counter < 3 && winGame == true) {
			System.out.println("Bravo pour cette mission : Voici 500 euros");
		}
		else {
			System.out.println("Vous pouvez toujours rejouer, malgès tout");
		}
		System.out.println("Merci d'avoir jouer.  Au revoir.");
	}


	/**
	 *  Affiche le message d'accueil pour le joueur.
	 */
	//@ pure
	public void afficherMsgBienvennue() {
		System.out.println();
		System.out.println("Bienvennue dans le jeu Abysmal !");
		System.out.println("Abysmal est un nouveau jeu d'aventure, enuyeux.");
		System.out.println("Tapez 'aide' si vous avez besoin d'aide.");
		showWin();
		System.out.println();
		System.out.println(current.descriptionLongue());
		this.player.get(0).showPlayer();
	}


	/**
	 *  Exécute la commande spécifiée. Si cette commande termine le jeu, la valeur
	 *  true est renvoyée, sinon false est renvoyée.
	 *  La fonction reset ne marche pas (en cours de code factoring).
	 *
	 * !!!!!!!!!!!!!!!! CODE A REFACTORER CAR TROP LONG !!!!!!
	 * Edit, en faite je vais rien toucher ici, jsuis pas motivé à me tapper tout
	 * ca
	 *
	 * @param  commande  La commande a exécuter
	 * @return           true si cette commande termine le jeu ; false sinon.
	 */
	/*@ requires commands != null
	  @ pure
	  @*/
	 public boolean traiterCommande(Commande commande) {
		 spacer();
 		if (commande.estInconnue()) {
 			System.out.println("Je ne comprends pas ce que vous voulez...");
 			return false;
 		}

		if (last != null)
			if (validatorCounter < 3 && !(last.equals(tt)))
				validatorCounter = 0;

 		String motCommande = commande.getMotCommande();
 		if (motCommande.equals("aide")) {
 			afficherAide();
			return false;
 		}
		else if (motCommande.equals("aller")) {
 			goToNextPiece(commande);
			if (this.current.equals(tt)
			   || this.tt.equals(last)) {
				return false;
			}
			validatorCounter++;
			goToNextPieceAI();
 		}
		else if (motCommande.equals("argent")) {
			this.player.get(0).showMoney();
			return false;
		}
		else if (motCommande.equals("retour")) {
			returnToLastPiece();
			validatorCounter--;
			returnToLastPieceAI();
		}
		else if (motCommande.equals("cplayer")) {
			System.out.print("Vous êtes : ");
			this.player.get(0).showPlayer();
			return false;
		}
		else if (motCommande.equals("bag")) {
			this.player.get(0).displayBag();
			return false;
		}
		else if (motCommande.equals("prendre")) {
			takeItem(commande);
			return false;
		}
		else if (motCommande.equals("laisser")) {
			dropItem(commande);
			return false;
		}
		else if (motCommande.equals("lister")) {
			showObjectPiece();
			current.showPlayerOnPiece();
			return false;
		}
		else if (motCommande.equals("but")) {
			showWin();
			return false;
		}
		else if (motCommande.equals("money")) {
			System.out.println("Vous avez : " + this.current.getUser().getMoney()
			+ "€");
			return false;
		}
		else if (motCommande.equals("sorties")) {
			showSorties();
			return false;
		}
		else if (motCommande.equals("check")) {
			showWin();
			validatorCounter++;
			return checkWinLoose();
		}
		else if (motCommande.equals("valider")){
			validerNavigo(this.player.get(0), current);
			return false;
		}
		else if (motCommande.equals("montrer")) {
			if( (commande.aSecondMot()) && (commande.aThirdMot()) )
				if (commande.getSecondMot().equals("navigo")) {
					if (commande.getThirdMot().equals("RATP")) {
						return showNavigo(this.player.get(0),
						                  this.player.get(0).getNavigo() , current);
					}
					else {
						System.out.println("Erreur, présenter le navigo à qui ?" +
						"(seulement controleur \"RATP\")");
					}
					return false;
				}
				else {
					return showNavigo(this.player.get(0), null , current);
				}
			else {
				System.out.println("Montrer quoi (Seulement navigo) ?");
				return false;
			}
		}
		else if (motCommande.equals("attendre")) {
			attendre();
			goToNextPieceAI();
			showSorties();
			return false;
		}
		else if (motCommande.equals("quitter")) {
 			if (commande.aSecondMot()) {
 				System.out.println("Quitter quoi ?");
 			}
			else {
 				return true;
 			}
 		}
		attendre();
 		return false;
 	}

	/**
	 * Print all the exit possible
	 */
	/*@
	  @ pure
		@*/
	void showSorties () {
		System.out.println(this.current.descriptionSorties());
	}
	/**
	 *  Affichage de l'aide. Affiche notament la liste des commandes utilisables.
	 */
	//@ pure
	public void afficherAide() {
		System.out.printf("Vous êtes en période de vacance, mais vous avez choisi "+
		"de travailler pour l'UFR SMBH dans le but de pouvoir payer votre loyer " +
		"CROUS et d'acheter les pates Carrefour Discount situé à Drancy Avenir. " +
		"Etant donné que le CROUS n'est pas capable de vous envoyer la bourse dans"+
		" les temps.\n\n" +
		"Le but de ce jeux est de récolter un certain nombre d'objet dont le poids"+
		" totale devra être exactement le même que celui généré par le jeu afin de"+
		" bénéfichier la paye. Il faudra déposer ces objets au secrétariat\n\n" +
		"Attention à bien valider votre pass navigo aussi, les controleurs sont "+
		"très présent ! Une amende vaudras 50 €\n\n\n" +
		"Etape 1 : récuperer votre carte etudiant chez vous en prenant le tram\n"+
		"Etape 2 : Aller a la fac pour commencer à collecter les objects\n" +
		"Etape 3 : Déposer les objects au secrétariat.\n",
		"Easy non ?\n");
		System.out.print("Le but actuelle  : ");
		showWin();
		System.out.println();
		System.out.println("Les commandes reconnues sont:");
		analyseurSyntaxique.afficherToutesLesCommandes();
	}

	/**
	 *  Tente d'aller dans la direction spécifiée par la commande. Si la piece
	 *  courante possède une sortie dans cette direction, la piece correspondant a
	 *  cette sortie devient la piece courante, dans les autres cas affiche un
	 *  message d'erreur.
	 *
	 * @param  commande Commande dont le second mot spécifie la direction a suivre
	 */
	/*@ requires commande != null
	  @ pure
    @*/
	public void goToNextPiece(Commande commande) {
		String direction = commande.getSecondMot();
		Piece next = current.next(direction);
		Joueur player = current.getUser();

		if (!commande.aSecondMot()) {
			System.out.println("Aller où ? (Précisez la sortie en argument)");
			System.out.println(current.descriptionSorties());
			return;
		}

		if (next == null) {
			System.out.println("Il n'y a pas de chemin dans cette direction!");
			return;
		}
		current.removeUser(player);
		last = current;

		if (next.descriptionCourte().equals("Teleportation")){
			current = ramdomizerPiece(pieceArray);
		}
		else {
			current = next;
		}
		current.addUser(player);
		System.out.println(current.descriptionLongue());
	}

	/**
	 * Un controleur peut se déplacer seulement sur les gare et dans le tram
	 */
	/*@
	  @ ensures currentAI != null
		@ ensures lastAI != null
		@ requires currentAI.getAI() != null
		@*/

	public void goToNextPieceAI() {
		Piece next = null;
		JoueurAI AI = currentAI.getAI();
		if ( currentAI.equals(dA) || currentAI.equals(lC) ) {
			next = tt;
			tt.addAI(AI);
			currentAI.removeAI(AI);
			lastAI = currentAI;
			currentAI = next;
			return;
		}

		else if (currentAI.equals(tt)) {
			if (lastAI.equals(dA) || lastAI.equals(lC)) {
				lastAI = tt;
			}
			else {
				next = currentAI.next("est");
				next.addAI(AI);
				currentAI.removeAI(AI);
				currentAI = next;
			}
		}
	}

	/**
	 * Methode return to the last visited piece.
	 * It can only return once. Since it set last = null
	 */
	//@ pure
	public void returnToLastPiece() {
		if (last != null) {
			Joueur player = current.getUser();
			current.removeUser(player);
			current = last;
			current.addUser(player);
			last    = null;
			System.out.println("Vous êtes retournée a la piece precedente");
		}
		else {
			System.out.print("Vous êtes déjà revenue à la derniere pièce ou bien");
			System.out.println(" vous n'avez pas encore effecuté de déplacement");
		}
		System.out.println(current.descriptionLongue());
		return;
	}

	/**
	 * Methode return to the last visited piece.
	 * It can only return once. Since it set last = null
	 */
	//@ pure
	public void returnToLastPieceAI() {
		if (this.lastAI.equals(tt)) {
			attendre();
		}
		JoueurAI AI = this.currentAI.getAI();
		this.currentAI.removeAI(AI);
		this.currentAI = this.lastAI;
		this.currentAI.addAI(AI);
		lastAI = null;
	}

	/**
	 * Add a piece into the piece ArrayList
	 * @param p the piece to be set into the array
	 */
	/*@ requires commandes != null
    @ pure
	  @*/
	public void addPiece(Piece p) {
		pieceArray.add(p);
	}

	/**
	 * Retrive a carryable item from the room to the bag
	 * Added security in case of String bug
	 * @param  o A Zork Object to put in the bag
	 */
	 /*@ requires o != null
		 @ pure
		 @*/
	public void retriveItem(ObjectZork o) {
		int size = this.current.getSize();
		int i = -1;
		while (++i < size) {
			if (this.current.getObject(i).equals(o) == true) {
				if (this.player.get(0).isAdd(o) == true) {
					this.current.removeObject(o);
					System.out.println("Vous avez mis dans votre sac " + o.getName());
					return;
				}
				else {
					System.out.println("Erreur, cette objet n'est pas transportable");
					return;
				}
			}
		}
		System.out.println("Erreur, cette objet n'est pas dans cette piece");
		return;
	}

	/**
	 * Deposite a Zork item from the bag to the room
	 * @param o A Zork Item
	 */
	 /*@ requires o != null
		 @ pure
		 @*/
	public void depositeItem(ObjectZork o) {
		int size = player.get(0).getBagSize();
		int i = -1;

		while (++i < size) {
			if (this.player.get(0).getBagItem(i).equals(o)) {
				this.player.get(0).removeItem(o);
				this.current.addObject(o);
				System.out.println("Vous avez déposé " + o.getName());
				return;
			}
			else {
				continue;
			}
		}
	}

	/**
	 * This method check if the name typed match to a object in the room.
	 * If yes, just apply the retriveItem, Else, it doesn't exist
	 * @param c Buffered command
	 */
	 /*@ requires c != null
		 @ pure
		 @*/
	public void takeItem(Commande c) {
		ObjectZork o = nameInArray(c.getSecondMot(), 1);

		if (!c.aSecondMot()) {
			System.out.println("Prendre quoi ? précisez l'objet en arg");
			return;
		}
		if (o != null) {
			retriveItem(o);
		}
		else {
			System.out.println("Erreur : Objet non trouvé dans la piece");
		}
	}

	/**
	 * Drop a item to the room, after checking that item is in the bag
	 * @param  c Name of the item
	 */
	 /*@ requires c != null
		 @ pure
		 @*/
	public void dropItem(Commande c) {
		ObjectZork o = nameInArray(c.getSecondMot(), 2);
		if(!c.aSecondMot()) {
			System.out.println("Laisser quel objet ?");
			return;
		}
		if(o != null) {
			depositeItem(o);
		}
		else {
			System.out.println("Objet dans le sac non trouvé");
		}
	}

	/**
	 * Check if a Object is in the array or nah
	 * @param  o          A Zork Object
	 * @return            A boolean statement
	 */
	 /*@ requires o != null
		 @ pure
		 @*/
	public boolean itemInArray(ObjectZork o) {
		return this.current.objectInList(o);
	}

	/**
	 * Check if the name match to a object, if yes, we just return the object
	 * @param  o   The name of the object
	 * @param  b   boolean statement for getting either bag object, or room object
	 * @return     A matching object
	 */
	 /*@ requires o != null
	   @ requires b != null
		 @ ensures \result == ObjectZork
		 @ pure
		 @*/
	public ObjectZork nameInArray(String o, int b) {
		int size;
		int i = -1;

		if (b == 1) {
			size = this.current.getSize();
			while (++i < size) {
				if (current.getObject(i).getName().equals(o)) {
					return this.current.getObject(i);
				}
				continue;
			}
			return null;
		}
		else {
			size = this.player.get(0).getBagSize();
			while (++i < size) {
				if (this.player.get(0).getBagItem(i).getName().equals(o)) {
					return this.player.get(0).getBagItem(i);
				}
				continue;
			}
			return null;
		}
	}

	/**
	 * Display all the Object from the current piece
	 */
	//@pure
	public void showObjectPiece() {
		int i = -1;
		int size = this.current.getSize();
		ObjectZork o;

		if (size == 0) {
			System.out.println("Il y a pas d'objet dans cette piece");
			return;
		}
		while (++i < size) {
			o = this.current.getObject(i);
			o.showObjectInfo();
			continue;
		}
		return;
	}

	/**
	 * Randomly insert an object, Randomly x times for each room
	 * @param pieceArray An ArrayList of pieceArray
	 * @param defaultO   An ArrayList of ObjectZork
	 */
	/*@ requires pieceArray != null
	  @ requires defaultO != null
	  @ pure
		@*/
	public void randomPieceObjectInsert(ArrayList<Piece> pieceArray,
	                                    ArrayList<ObjectZork> defaultO) {
		int i = -1;
		int j = -1;
		int ranObjSize = randomWithRange(1,11); // Aléatoire entre 0 et 11
		int trueSizePiece = pieceArray.size();
		ObjectZork o;

		while (++i < trueSizePiece - 1) {
			while (++j < ranObjSize) {
				o = randomizerObject(defaultO);
				pieceArray.get(i).addObject(o);
			}
			ranObjSize = randomWithRange(1,11);
			j = -1;
			continue;
		}
		mA.addObject(carteEtudiante);
	}

	/**
	 * Return the calculus of a randomized object
	 * @param   defaultO      ArrayList of all ObjectZork
	 * @return                A randomized ObjectZork
	 */
	/*@ requires defaultO != null
	  @ ensures \result == ObjectZorku
		@ pure
		@*/
	public ObjectZork randomizerObject (ArrayList<ObjectZork> defaultO) {
		int ran = randomWithRange(0, 14);
		return defaultO.get(ran);
	}

	/**
	 * Randomize a value between 0 and 10 and return randomized piece
	 * @param   p             An ArrayList of piece
	 * @return                A randomized piece between 0 and 10.
	 */
	/*@ requires p != null;
 	  @ pure
 	  @*/
	public Piece ramdomizerPiece (ArrayList<Piece> p) {
		int rand = randomWithRange (0, 10);
		return p.get(rand);
	}

	/**
	 * Random calculator between max and min
	 * @param  min           A minimum integer
	 * @param  max           A maximum integer
	 * @return               A Randomized integer between min and max
	 */
	/*@
	  @ requires int min != null && max != null
	  @ ensures \result == (int)(Math.random() * range) + min;
	  @ pure
	  @*/
	int randomWithRange(int min, int max) {
   int range = (max - min) + 1;
   return (int)(Math.random() * range) + min;
	}

 /*---------------------------------------------------------------------------/

 /**
  * Check if the "Check" command is valide for winning or nah.
  * If true and the counter is below 3, so we win
  * If false and counter below 3, warning message for the exact weight of bag
  * If false and over 3, game over
  * @return a boolean statemnt
  */
	/*@
	  @ ensures \result == (player.get(0).getWeight() == win) && counter < 3;
		@ ensures \result != (player.get(0).getWeight() == win)
	  @ pure
	  @*/
	public boolean checkWinLoose() {
		if (current.descriptionCourte().equals("le secrétariat") && counter < 4) {
			if (player.get(0).getWeight() == win && player.get(0).doIhaveIt(carteEtudiante)) {
				System.out.print("Vous avez réussi à déposé le sac au secrétariat ");
				System.out.println( win + " kg, vous avez fini votre mission bravo");
				this.winGame = true;
				return true;
			}
			else {
				if (counter < 3) {
					System.out.print("Vous devez avoir exactement " + win + " kg dans");
					System.out.println(" votre sac");
					System.out.println("Vous devez être en possession de votre carteEtudiante");
					if (player.get(0).doIhaveIt(carteEtudiante))
						System.out.println("Vous l'avez");
					else
						System.out.println("Vous l'avez pas");
					counter++;
					System.out.println("C'est votre " +counter+ " tentative");
					return false;
				}
				else {
					System.out.print("Vous avez soulé la secrétaire " + counter + " fois");
					System.out.println(". Vous êtes dorénavant viré. So comman le loyer?");
					return true;
				}
			}
		}
		else {
			System.out.print("Vous devez être au secrétariat pour effectuer cette ");
			System.out.println("commande");
		}
		System.out.println(current.descriptionLongue());
		return false;
	}

	/**
	 * Display the ammount of exact weight of the bag in order to win the game
	 */
	//@ pure
	public void showWin() {
		System.out.println("Il faut déposer un sac de " +win+ "kg au secrétariat");
	}

 /*---------------------------------------------------------------------------*/

	/**
	 * Validation card transportation into the tram
	 * @author Rayan Fraj
	 * @param j Un joueur
	 * @param p La piece actuelle
	 */
	/*@
	  @ requires j != null && p != null
		@ pure
		@*/
	public void validerNavigo(Joueur j, Piece p) {
		if (p.getDescription().equals("Tramway T1 (101)")) {
			j.getNavigo().setValidation();
			System.out.println("Passe navigo validé, bon voyage");
			return;
		}
		System.out.println("Vous devez être dans un tramway pour valider");
		return;
	}

	/**
	 * Show card transportation to the card controller
	 * @author Rayan Fraj
	 * @param  j             A player
	 * @param  c             Card Navigo
	 * @param  p             Current Piece
	 * @return               A boolean statement
	 */
	/*@
	  @ requires j != null && piece != null
		@ ensures controle = {-1,0,1}
		@*/
	public boolean showNavigo(Joueur j, CarteNavigo c, Piece p) {
		int controle = p.getAI().controleNavigo(j, c);

		if (controle == -1) {
			System.out.println("Merci de présenter votre titre de transport SVP !");
			return false;
		}

		if (controle == 0) {
			return true;
		}

		return false;
	}

	/**
	 * Permet juste le déplacement mobile du tramway
	 */
	/*@
	  @ pure
		@*/
	public void attendre() {
		if (tt.getSortie().containsValue(dA)) {
			System.out.println("Le tramway était à Drancy Avenir"+
			" Il est maintenant à la courneuve");
			tt.setSorties(null, lC, null, null, null, null);
			dA.setSorties(pi1, pi2,   TP, null, null, null);
			lC.setSorties(null, mA, null,   tt, null, null);
		}
		else if (tt.getSortie().containsValue(lC)) {
			System.out.println("Le tramway était à La Courneuve"
			+ " Il est maintenant à Drancy");
			tt.setSorties(null, dA, null, null, null, null);
			dA.setSorties(pi1, pi2,   TP,   tt, null, null);
			lC.setSorties(null, mA, null, null, null, null);
		}
	}

	/**
	 * Add blank space between each interraction
	 */
	//@ pure
	public void spacer() {
		System.out.printf("\n\n\n");
	}
}
