/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ObjectZork.java                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/04 18:13:31 by NoobZik           #+#    #+#             */
/*   Updated: 2017/11/15 09:28:04 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

import java.util.Objects;

/**
 * This part is just about how a Object is defined<p>
 * The following object is defined by
 * - <br> A name identifier
 * - <br> A Short
 * - <br> A weight
 * - <br> If this object can be carried by the player or nah</p>
 * @author Rakib SHEIKH
 * @since v0.0.1
 * @author Rayan
 */

public class ObjectZork extends NamingDescription{
  private boolean   carry;


  /**
   * Constructor Default ObjectZork
   * @author Rayan
   */
  public ObjectZork() {
    super("A Default name",
          "A Default description");
    this.setWeight(0);
    this.carry  = false;
  }

  /**
   * Personalized Constructor for ObjectZork
   * @param  n             A name
   * @param  d             A description of the object
   * @param  w             A integer weight
   * @param  c             A boolean carry
   */
  /*@
    @ requires n != null && d != null && w != null && c != null;
    @ ensures name = getName();
    @         description = getDescription();
    @         w = getWeight();
    @         c = getCarry();
    @ pure
    * @author Rayan
    @*/
  public ObjectZork(String n, String d, int w, boolean c) {
    super(n, d);
    this.setWeight(w);
    this.carry = c;
  }

  /**
   * Set an boolean to the current object
   * @param c The boolean to be setted into the object
   */
  /*@
     @ requires c != null
     @ ensures carry = getCarry();
     @ pure
     @*/
  private void setCarry(boolean c) {
    this.carry = c;
  }

  /**
   * Get the status of the current object if it's carrable or nah.
   * @return            The true or false statement from the carry field.
   */
  /*@
    @ ensures \result this.carry
    @ pure
    @*/
  public boolean getCarry() {
    return this.carry;
  }

  /**
   * Print all Object related information
   */
  /*@ pure @*/
  public void showObjectInfo() {
    System.out.print("Nom :" +getName()+", Description :"+getDescription() );
    System.out.println("Poids : "+getWeight()+" kg");
  }

  /**
   * Compare two Zork Object
   * If the description, weight and carry are equals, the two object are equals.
   * @param o 2nd object to compare with
   * @return a boolean statement
   */
  /*@ pure
    @ ensures !(o instanceof ObjectZork) ==> !\result;
    @ ensures  (o instanceof ObjectZork) ==> !\result;
    @ \result == (getDescription() == ((ObjectZork) oz).getDescription())
    @             && (getCarry() == ((ObjectZork) oz).getCarry())
    @             && (getWeight() == ((ObjectZork) oz).getCarry())
    @*/
  @Override
  public boolean equals(Object o) {
		if(!(o instanceof ObjectZork))
			return false;

		ObjectZork oz = (ObjectZork) o;

    if (this.getDescription().equals(oz.getDescription() )
      && this.getWeight() == oz.getWeight()
      && this.getCarry()  == oz.getCarry())
			return true;
    return false;
	}

  /**
   * Since we have a customized equals, we have to customize also the hashCode
   * since equals and hashcode are close related.
   * IF YOU EDIT EQUALS, YOU ALSO HAVE TO EDIT HASHCODE
   * @return A generated hashCode
   */
  @Override
  public int hashCode() {
    return Objects.hash(getName(),getDescription(),getWeight(),carry);
  }
}
