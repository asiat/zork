/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NamingDescription.java                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/14 23:02:18 by NoobZik           #+#    #+#             */
/*   Updated: 2017/11/15 10:07:36 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
 * @since 7395d17333db9cba6b44bf5a745f46fca5f27775
 * @author Rayan
 */
class NamingDescription {
  private String    name;
  private String    description;
  private int       weight;

  /**
   * Customized Constructor
   * @param  String n             [description]
   * @param  String d             [description]
   * @return        [description]
   */
  /*@
    @ ensures name = getName();
    @ ensures description = getDescription()
    @ requires n != null
    @ requires d != null
    @*/
  public NamingDescription (String n, String d) {
    this.name        = n;
    this.description = d;
  }

  /**
   * set the given Weight
   * @param w An Weight integer
   */
  /*@
    @ requires w != null
    @ ensures this.weight = getWeight();
    @*/
  public void setWeight (int w) {
    this.weight = w;
  }

  /**
	 * Sum of the current weight of the player to the imported object.
	 * @param w Imported weight of the Imported Zork Object.
	 */
	/*@
	  @ requires (weight != null && w != null);
		@ ensures  (this.weight != null);
		@ pure
	        * @author Rayan
	  @*/
  public void setAppendWeight (int w) {
    this.weight += w;
  }

  /**
   * Retrieve the current weight
   * @return an Weight integer
   */
  /*@
    @ requires this.w = getWeight();
    @ pure
    @*/
  public int getWeight () {
    return this.weight;
  }

  /**
   * Set a name to the current object
   * @param n The current name to be setted
   */
  /*@
    @ requires n != null
    @ ensures name = getName();
    @ pure
    @*/
  public void setName(String n) {
    this.name = n;
  }

  /**
   * Get the name of the current object
   * @return            The String name of the current object
   */
  /*@
    @ ensures \result this.name
    @ pure
    @*/
  public String getName() {
    return this.name;
  }

  /**
   * Set a description to the player, just in case it doesn't suit to him
   * @param d A String formatted escription.
   */
  /*@
    @ pure
    @*/
  public void setDescription(String d) {
    this.description = d;
  }

  /**
   * Retrive the Description of the player, intended for displaying it.
   * @return A string formatted description.
   */
  /*@
    @ requires (* this.getDescription not nulled *);
    @ pure
    @*/
  public String getDescription() {
    return this.description;
  }
}
