# README : Projet Zork #

Projet rédigé en Java orienté objet.

Merci de ne pas paniqer en voyant un makefile (Je suis un flemmard du terminal).
Le jeu s'execute par:
```
$ java Zorkfr.java
# ou
$ make && make run
```

## Version actuel : 0.06 (Taggé Alpha 3) ##
Voir rapport ou commit history

### Changelogs ###
#### Alpha 02 ####
*   Implémentation de Joueur.Java
*   Divers modifications de piece.java et ObjectZork.java
*   Ajout des commandes supplémentaires
*   Ajout d'une pièce téléportation
*   (Liste non complète).


#### 0.02 ####

~Modification des pieces (Campus de Bobigny)
*   Ajout ObjectZork
*   Ajout Joueur
*   Modifications des sorties (Haut et Bas)
*   Fixer tout les warnings de quelqu'un qui sait pas coder correctement (Champesme).
*   Rapport mis-à-jour.

#### 0.01 ####
+Mise en repertoire d'un projet Zork
+Ajout de README.md
+Ajout d'un Report.md

## Ce qui reste à faire ##

### Objectif ###

-   [x] Fixer les warnings d'un prof qui sais pas coder correctement ! (Fait)
-   [ ] Indenation fix Google / 42.fr
-   [ ] Commentaires respectant les normes de prog Google.
-   [ ] **Trouver la motivation à rédiger les contrats de classe/méthodes**
-   [ ] Refactorer la fonction traiterCommande (en maximum 25 lignes).
-   [x] Interconnecter ObjectZork dans Jeu.
-   [x] Interconnecter Joueur dans Jeu.
-   [x] Etablissement d'une liste des ObjectZork.
-   [x] Generation de manière aléatoire la liste des objets dans chaque pièces.
-   [x] Faire les exos de TP. (mdr jamais ! Vive Udacity)
-   [x] Faire le sujet.
-   [ ] Ajouter une aide detaillé (aide "commande") pour éviter d'aller dans le rapport
-   [ ] **Intelligence Artificiel de type Udacity ?**
-   [x] **IMPORTANT : Factoriser tout les whiles répétitifs (Avec une méthodes qui
  retourne la position de l'objet ? A tester)**
-   [ ] Faire une fonction qui fait une auto-capitalisation.

### Sujet demandé Partie 1 ###

***La premiere partie est terminé***

-   [x] Certaines pieces contiennent des objets. Chaque pièce peut contenir un
        nombre arbitraire d’objets. Certains objets peuvent etre emportés par le
        joueur, d’autres non.
-   [x] Le joueur peut transporter des objets avec lui. Chaque objet a un poids.
        Le joueur ne peut transporter des objets que dans la mesure ou le poids
        total des objets transportés ne dépasse pas certaine limite fixée à
        l’avance.
-   [x] Le joueur peut gagner. Certaines situations doivent pouvoir être
        reconnues comme des situations gagnantes dans lesquelles le joueur sera
        averti qu’il a gagné.
-   [x] Implementez une commande “retour” qui fait revenir le joueur dans la
        pièce précédemment visitée.
-   [x] Ajoutez au moins quatre nouvelles commandes (en plus de celles déjà
        présentes dans le code qui vous est fourni).
-   [x] J'ai pris trop d'avance, la partie 2 est quasiment fini en faite...
(il manque les controleurs du T1 a modéliser et les commandes a 3 args).

***Deuxième partie est terminé***
-   [x] Modélisations des controleurs
-   [x] Mise au point des classe d'héritage
-   [x] Simplification totales des boucles
-   [x] Troisième commandes

***

## Méthodes de programmation ##

Il faut absolument **mettre des commentaires dans vos programmes**, surtout dans
la rédaction de vos fonctions.

# [On utilise la norme de Google](https://google.github.io/styleguide/javaguide.html) #

Les commentaires pour les fonctions auront le style suivant :
```java
/**
 * Nom de la fonction + Description de la fonction
 * Complexité : Espace O() - Temps O()
 * @param  variable_v description variable
 * @return "juste le type" description return
 */
```
Un exemple :
```java
/**
 * case_select(case_t case_v)
 * Permet de connaitre si la case actuelle peut être sélectionnable par l'utilisateur
 * en fonction du type de pièce. Fait appel aux autres fonctions déplacement_diagonal /
 * pion / vertical / horizontal
 * @param   case_v Une coordonéee selectionné
 * @return  int    booleen pour selectionnable or nah
 */
```

***
# IMPORTANT #
**Si vous trouvez un bug, [Signalez le](https://bitbucket.org/asiat/zork/issues/new) sur le bitbucket, pour pas qu'on l'oublie.
L'objectif est 0 Warnings + 0 Erreurs  (segmentation fault + pas le même nombre de malloc et free) + 0 bug / comportement fausse du programme**.

***Un warning est considéré comme une erreur***

*   Pour signaler le(s) bug(s), reportez vous dans la partie [Signalement de bug](https://bitbucket.org/asiat/prog_imp_2017/issues?status=new&status=open)
*   S'applique aussi pour les erreurs, warnings que vous n'arrivez pas à fixer.

On évite aussi de travailler sur le même ficher, pour éviter les conflits de push (push veut dire mettre en ligne les modifications vers le répertoire git).

## Gestion du git ##
Je vous conseil d'installer GitKraken pour la gestion du répertoire en ligne. L'utilisation des clées USB est strictement interdite.

### Téléchargement du répertoire du projet ###

Il y a deux manière de récupérer les données du répertoire :

#### Méthode 1 ####
Récupérez vos identifiants SSH en haut à droite de la page du répertoire.
Ensuite dans GitKraken, Allez dans *Fichiers -> Cloner (Ou Crt+N)* puis clonez sur le pc.

#### Méthode 2 ####
Une autre méthode que j'ai découvert avec la nouvelle version, il suffit tout simplement de raccorder son compte bitbucket sur Gitkraken, ensuite vous pouvez directement cloner le répertoire depuis la liste des répertoires disponible.

Vous pouvez désormais commencer à modifier les codes sources du programme normalement.

### Procédure de mise en ligne des modification (commit) : ###

On effectue un commit seulement pour une fonctionnalité fini. C'est plus simple ensuite à retracer l'evolution d'un codage merdique.

1.  'Stage all changes' (ou quelque fichier) les fichiers que avez modifié
2.  Ecrire un titre dans le commit + Un commentaire détaillant ce que vous avez fait

    *   ***IMPORTANT écrivez pas de la merde, s'il y a un problème, on va galérer a revenir en arrière !***

3.  Stage files /changes to Commit (Le bouton vert qu'il y a en bas du box)
4.  Push (Dans la bar d'outil au dessus, l'icone sous forme de flèche vers le haut avec un trait au dessus)
5.  ???
6.  Voila !

On pourra savoir qui a modifié/créer quoi, il y a un historique qui se trouve [sur ce lien](https://bitbucket.org/asiat/zork/commits/)

**Note Important : Oubliez pas de supprimer les fichiers .class avant de push, la commande make clean est la pour ça.**

**De toute façon, il y a un .gitignore qui est là au cas où**

Pour récuperer/synchro les données vers la version récente:
-   Pull (icone avec une flèche vers le bas, un trait un bas, il se trouve sur la barre d'outils)

## Gestions des branches ##
*   La *branche principale* est le ***master*** C'est la version principale stable qui sera rendu aux profs.
*   Les *branches secondaires* seront à créer pour les development des fichiers
*   Avant de créer des fonctions, on doit impérativement utiliser les branches secondaires (Allez dans Branches -> Créer Branches -> Le nom de la branche)

Pour changer de branche :
Il y a jusqu'à double cliquer parmi la liste qui se situe à gauche
Je m'occupe de merge les modification vers la version principale après avoir testé.

***

## Atom ##

Je vous conseil fortement de coder avec [Atom](http://atom.io) en installant les plugins suivant :

*   Pour éviter de passer sur le terminal pour regler les Warning et les erreurs [linter](https://atom.io/packages/linter) et [linter-javac](https://atom.io/packages/linter-javac).
*   Pour vous retrouver dans le programme long [minimap](https://atom.io/packages/minimap).
*   Pour vous retrouver dans les fichiers, [files-icons](https://atom.io/packages/file-icons).
*   Pour être un master de la flemitude des commentaire [Docblockr](https://atom.io/packages/docblockr).
*   Pour afficher les couleurs d'erreurs dans la minimap [minimap-linter](https://atom.io/packages/minimap-linter).

*   Pour rédiger les rapport comme moi, Markdown themable pdf
